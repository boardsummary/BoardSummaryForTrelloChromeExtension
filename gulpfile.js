'use strict';

// Required modules
const { exec } = require('node:child_process');
const { rm } = require('node:fs/promises');
const { join } = require('node:path');

const { series, parallel, src, dest } = require('gulp');
const cleanCSS = require('gulp-clean-css');
const gulpif = require('gulp-if');
const uglify = require('gulp-uglify-es').default;
const log = require('gulplog');
const merge = require('merge-stream');
const minimist = require('minimist');

// Configuration settings
const allFilesFolders = '/**/*';
const sourceDir = 'src';
const sourceFiles = sourceDir + allFilesFolders;
const sourceScriptsDir = `${sourceDir}/scripts`;
const sourceScriptFiles = `${sourceScriptsDir}/*.js`;
const sourceStyleFiles = `${sourceDir}/styles/*.css`;
const sourceScriptTemplatesDir = `${sourceScriptsDir}/templates`;
const sourceScriptTemplateFiles = `${sourceScriptTemplatesDir}/*.handlebars*`;
const sourceExclusions = [
  `${sourceDir}/jsconfig.json`,
  sourceScriptTemplatesDir,
  sourceScriptTemplateFiles
];
const releaseDir = 'build';
const releaseScriptsDir = `${releaseDir}/scripts`;
const releaseStylesDir = `${releaseDir}/styles`;
const precompiledTemplatesFile = `${releaseScriptsDir}/templates.min.js`;
const externalDependencies = [
  {
    dest: releaseDir,
    name: 'license',
    src: 'license'
  },
  {
    dest: `${releaseScriptsDir}/jquery`,
    name: 'jquery',
    src: [
      'node_modules/jquery/dist/jquery.min.js',
      'node_modules/jquery/LICENSE.txt'
    ]
  },
  {
    dest: `${releaseScriptsDir}/handlebars`,
    name: 'handlebars',
    src: [
      'node_modules/handlebars/dist/handlebars.runtime.min.js',
      'node_modules/handlebars/LICENSE'
    ]
  }
];
const buildSettings = {
  defaultBuildEnvironment: 'master', // Default environment is production
  versionTag: /v(?<major>\d+)\.(?<minor>\d+)\.(?<patch>\d+)/ // Releases are tagged with v0.0.0
};

// Get branch name from env parameter
const getBranchName = (args, settings) => {
  const knownOptions = {
    default: { env: settings.defaultBuildEnvironment },
    string: 'env'
  };
  const sliceStart = 2; // 0: node, 1: gulp, 2: task name or arguments
  return minimist(args.slice(sliceStart), knownOptions).env;
};
const branchName = getBranchName(process.argv, buildSettings);

// Build is production if on the default branch or is tagged as a release version
const checkProductionBuild = (branch, settings) =>
  branch === settings.defaultBuildEnvironment ||
  settings.versionTag.test(branch);
const isProductionBuild = checkProductionBuild(branchName, buildSettings);

// Construct array of globs with all source files and exclusions
const getSourceFilesWithExclusions = () => {
  const result = [sourceFiles];
  sourceExclusions.forEach((exclusion) => {
    result.push(`!${exclusion}`);
  });
  return result;
};

// Deletes all files and folders from build directory
const clearBuildFolder = async () => {
  await rm(join(releaseDir, allFilesFolders), { force: true, recursive: true });
};

// Copies all required source files to the build folder
const copySourceFiles = () =>
  src(getSourceFilesWithExclusions()).pipe(dest(releaseDir));

// Pre-compile handlebars templates.  Ensure copySourceFiles is a dependency so the output folder exists.
const compileHandlebars = () =>
  exec(
    `"./node_modules/.bin/handlebars" -m ${sourceScriptTemplatesDir} -f ${precompiledTemplatesFile}`
  );

// Copies all dependent files to the build folder
const copyDependencies = () => {
  const result = merge();
  externalDependencies.forEach((dependency) => {
    log.info(` Copying '${dependency.name}' files...`);
    result.add(src(dependency.src).pipe(dest(dependency.dest)));
  });
  return result;
};

// Copy and minify CSS.  Ensure copySourceFiles is a dependency so that the original files are
// overwritten by the minified files.
const processCss = () => {
  log.info(
    ` Processing CSS for ${branchName}${
      isProductionBuild ? ' (minified)' : ''
    }...`
  );
  return src(sourceStyleFiles)
    .pipe(gulpif(isProductionBuild, cleanCSS())) // Only minify in production
    .pipe(dest(releaseStylesDir));
};

// Copy and minify scripts.  Ensure copySourceFiles is a dependency so that the original files are
// overwritten by the minified files.
const processScripts = () => {
  log.info(
    ` Processing scripts for ${branchName}${
      isProductionBuild ? ' (minified)' : ''
    }...`
  );
  return src(sourceScriptFiles)
    .pipe(gulpif(isProductionBuild, uglify())) // Only minify in production
    .pipe(dest(releaseScriptsDir));
};

const build = series(
  clearBuildFolder,
  parallel(copySourceFiles, copyDependencies),
  parallel(compileHandlebars, processCss, processScripts)
);

exports.build = build;
exports.default = build;
